using Pkg

Pkg.activate(dirname(dirname(@__DIR__)))
Pkg.build()
Pkg.test()
