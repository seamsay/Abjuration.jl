#!/usr/bin/env julia

if isempty(ARGS)
    commands = readdir(joinpath(@__DIR__, "commands")) .|> splitext .|> first

    println("Usage: $PROGRAM_FILE COMMAND")
    println("Available commands are:")
    for command in commands
        println('\t', command)
    end
else
    command = ARGS[1]
    path = joinpath(@__DIR__, "commands", command) * ".jl"
    arguments = ARGS[2:end]

    run(`julia --project=dev-scripts $path $arguments`)
end
