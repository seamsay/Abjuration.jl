# Abjuration

**A** **B**etter **Ju**lia **R**EPL ... **ation**?

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://seamsay.gitlab.io/Abjuration.jl/dev)
[![Build Status](https://gitlab.com/seamsay/Abjuration.jl/badges/master/pipeline.svg)](https://gitlab.com/seamsay/Abjuration.jl/pipelines)
[![Coverage](https://gitlab.com/seamsay/Abjuration.jl/badges/master/coverage.svg)](https://gitlab.com/seamsay/Abjuration.jl/commits/master)

## Planned Features

* Consistent keybindings.
    * `Enter` always inserts a newline.
    * `Ctrl+Enter` always executes the cell.
    * `Up` and `Down` always move withing an cell.
    * `PageUp` and `PageDown` always move between cells.
* Easy to develop modes.
    * There will be no "blessed functionality", anything done by the package could be just as easily done by an external package.
* Highly customisable prompt.
    * Function for printing something before the prompt.
    * Prompt string.
    * Continuation string.
    * Maybe a prompt function?
* Syntax highlighting.

## Sibling Packages

* Kakromancy: [Kakoune](https://kakoune.org) keybindings.
* Divimation: [Vim](https://www.vim.org) keybindings.
* Emacation: [Emacs](https://www.gnu.org/software/emacs) keybindings.