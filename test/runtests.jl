using Abjuration
using Documenter
using JuliaFormatter
using Test

# Style Tests!

function isformatted(path)
    if isfile(path)
        if last(splitext(path)) != ".jl" || success(`git check-ignore $path`)
            return true
        end

        original = read(path, String)
        formatted = format_text(
            original,
            always_for_in = true,
            whitespace_typedefs = true,
            whitespace_ops_in_indices = true,
            remove_extra_newlines = true,
        )

        original == formatted
    else
        isformatted(joinpath.(path, readdir(path)))
    end
end

isformatted(paths::Vector) = all(isformatted.(paths))

@testset "Formatting" begin
    @test isformatted(dirname(@__DIR__))
end

# Doc Tests!

DocMeta.setdocmeta!(Abjuration, :DocTestSetup, :(using Abjuration))
doctest(Abjuration)

# Unit Tests!

struct FakeREPL <: Abjuration.REPL end
shouldshow(value, input) = Abjuration.shouldshow(FakeREPL(), value, input)

@testset "Abjuration.jl" begin
    @testset "Should Show" begin
        @test !shouldshow(nothing, "println(1 + 1)")
        @test !shouldshow(2, "1 + 1;")
        @test shouldshow(";#", "\";#\"")
        @test shouldshow(3, "1 + 1; 2 + 1")
    end
end
