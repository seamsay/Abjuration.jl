module DumbREPLs

import ..accept
import ..REPL
using ..Ansillary: TTYTerminal

struct DumbREPL <: REPL
    terminal::TTYTerminal
end

preprompt(::DumbREPL) = ""
prompt(::DumbREPL) = "julia> "
continuation(::DumbREPL) = "~~~~~> "

function accept(repl::DumbREPL)
    print(repl.terminal.out_stream, preprompt(repl))

    input = ""
    prefix = prompt(repl)
    while true
        Base.reseteof(repl.terminal.in_stream)

        print(repl.terminal.out_stream, prefix)
        prefix = continuation(repl)

        line = try
            readline(repl.terminal.in_stream, keep = true)
        catch e
            if e isa InterruptException
                try
                    ccall(:jl_raise_debugger, Nothing, ())
                catch
                end

                println(repl.terminal.out_stream)
                return ""
            elseif e isa EOFError
                return nothing
            else
                rethrow()
            end
        end

        if !endswith(line, '\n')
            prefix = ""
        end

        input *= line

        # Ctrl+d on an empty line returns an emtpy string rather than raising `EOFError`.
        if isempty(input)
            return nothing
        end

        ast = Base.parse_input_line(input)
        if !isa(ast, Expr) || ast.head !== :incomplete
            return input
        end
    end
end

end # module
