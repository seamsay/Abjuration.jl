module Abjuration

using Ansillary
using Tokenize

"""
Use Abjuration instead of the standard library REPL module.

!!! warning

    I don't think the calls that Base makes when running the REPL are considered public APIs, so future versions of Julia might break this without warning.

!!! warning

    If anyone opens an issue about polymorph being a transmutation spell and not an abjuration spell, I swear to fucking God...
"""
polymorph!() = Base.REPL_MODULE_REF[] = StdLibCompat

abstract type REPL end

function accept end

goodbye(::REPL) = "Goodbye!"

parse(::REPL, input) = Meta.parse(input)

function shouldshow(::REPL, value, input)
    function ends_with_semicolon(input)
        tokens = collect(tokenize(input))

        semicolon = findlast(tokens) do token
            Tokens.kind(token) == Tokens.SEMICOLON
        end

        !isnothing(semicolon) && all(tokens[semicolon:end]) do token
            Tokens.kind(token) in
            (Tokens.SEMICOLON, Tokens.WHITESPACE, Tokens.COMMENT, Tokens.ENDMARKER)
        end
    end

    !(isnothing(value) || ends_with_semicolon(input))
end

include("DumbREPLs.jl")

mutable struct Evaluator
    request::Channel
    response::Channel
    """
    Whether input is currently being evaluated.

    This is required by `Base`.
    """
    in_eval::Bool
    """
    Task that is running the user input.

    This is required by `Base`.
    """
    backend_task::Task

    Evaluator(i, o, e) = new(i, o, e)
end

function Evaluator()
    request = Channel(1)
    response = Channel(1)
    evaluator = Evaluator(request, response, false)

    task = @async begin
        storage = task_local_storage()
        # Needed for `include` to know where to search. `nothing` indicates current directory.
        storage[:SOURCE_PATH] = nothing

        while true
            ast = take!(request)

            evaluator.in_eval = true
            try
                value = Core.eval(Main, ast)
                ccall(:jl_set_global, Nothing, (Any, Any, Any), Main, :ans, value)
                put!(response, (value, nothing))
            catch e
                put!(response, (e, catch_backtrace()))
            finally
                evaluator.in_eval = false
            end
        end
    end

    evaluator
end

function run(repl::REPL, evaluator::Evaluator)
    while true
        input = accept(repl)
        if input === nothing
            break
        end

        ast = parse(repl, input)

        put!(evaluator.request, ast)
        value, backtrace = take!(evaluator.response)

        if backtrace !== nothing
            Base.invokelatest(
                Base.display_error,
                repl.terminal.err_stream,
                value,
                backtrace,
            )
        elseif shouldshow(repl, value, input)
            Base.invokelatest(Base.display, value)
        end
    end

    message = goodbye(repl)
    if message !== nothing
        println(repl.terminal.out_stream, message)
    else
        run(repl, evaluator)
    end
end

function run(terminal = Ansillary.TERMINAL[])
    repl = if terminal.term_type == "dumb"
        DumbREPLs.DumbREPL(terminal)
    else
        DumbREPLs.DumbREPL(terminal)
    end

    run(repl, Evaluator())
end

include("StdLibCompat.jl")

end # module
