"""
Module that mimics the REPL standard library module.

This is needed so that Abjuration can be used as the main REPL for Julia.
"""
module StdLibCompat

module Terminals
using REPL.Terminals: TTYTerminal, hascolor
end

using ..DumbREPLs: DumbREPL
import ..Evaluator
import REPL
using REPL: LineEditREPL, REPLDisplay
import ..run

# Needed for `docview.jl`.
using REPL: REPLCompletions, lookup_doc

struct BasicREPL <: REPL.AbstractREPL
    inner::DumbREPL
    BasicREPL(term) = new(DumbREPL(term))
end

# Needed for `REPLDisplay`.
REPL.outstream(repl::BasicREPL) = repl.inner.terminal.out_stream

function run_repl(repl, consumer)
    if repl isa BasicREPL
        backend = Evaluator()
        consumer(backend)
        run(repl.inner, backend)
    else
        REPL.run_repl(repl, consumer)
    end
end

end # module
