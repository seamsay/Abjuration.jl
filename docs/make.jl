using Documenter, Abjuration

makedocs(;
    modules = [Abjuration],
    format = Documenter.HTML(),
    pages = ["Home" => "index.md"],
    repo = "https://gitlab.com/seamsay/Abjuration.jl/blob/{commit}{path}#L{line}",
    sitename = "Abjuration.jl",
    authors = "Sean Marshallsay",
    assets = String[],
)
